use crate::dns_record_api_error::DnsRecordApiError;

#[derive(Debug, PartialEq, Clone)]
pub struct ApiConfig {
    pub access_token: String,
    pub zone_id: String,
    pub domain: String,
}

impl ApiConfig {
    #[allow(unused)]
    pub fn read_from_env() -> Result<ApiConfig, DnsRecordApiError> {
        let access_token = match std::env::var("ACCESS_TOKEN") {
            Ok(value) => value.into(),
            Err(_) => return Err(
                DnsRecordApiError::Other("ACCESS_TOKEN not set")
            ),
        };

        let zone_id = match std::env::var("ZONE_ID") {
            Ok(value) => value.into(),
            Err(_) => return Err(
                DnsRecordApiError::Other("ZONE_ID not set")
            ),
        };

        let domain = match std::env::var("DOMAIN") {
            Ok(value) => value.into(),
            Err(_) => return Err(
                DnsRecordApiError::Other("DOMAIN not set")
            ),
        };
        return Ok(ApiConfig {
            access_token,
            zone_id,
            domain,
        });
    }
}