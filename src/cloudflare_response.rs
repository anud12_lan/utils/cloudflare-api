use serde::Serialize;
use serde::Deserialize;


#[derive(Debug, Serialize, Deserialize)]
pub struct CloudflareResponse<T> {
    pub success: bool,
    pub result: T,
}

impl<T> CloudflareResponse<T> {
    pub fn map_result<U>(self, func: &mut dyn FnMut(T) -> U) -> CloudflareResponse<U> {
        CloudflareResponse {
            result: func(self.result),
            success: self.success,
        }
    }
}