use serde::Serialize;
use serde::Deserialize;

//Mapping for https://api.cloudflare.com/#dns-records-for-a-zone-properties
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DnsRecord {
    pub id: String,
    pub name:String,
    pub created_on: String,
}