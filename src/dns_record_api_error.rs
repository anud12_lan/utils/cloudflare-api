use std::fmt::{Display, Formatter};
use ureq::{Error};

#[derive(Debug)]
pub enum DnsRecordApiError {
    Other(&'static str),
    Request(Error),
}

impl From<Error> for DnsRecordApiError {
    fn from(error: Error) -> Self {
        DnsRecordApiError::Request(error)
    }
}

impl Display for DnsRecordApiError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DnsRecordApiError::Other(e) => e.fmt(f),
            DnsRecordApiError::Request(e) => e.fmt(f)
        }
    }
}