pub mod api_config;
pub mod dns_record_api;
mod dns_record;
mod cloudflare_response;
mod dns_record_api_error;


#[cfg(test)]
mod runnable {
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
