use serde_json::Value;
use crate::cloudflare_response::CloudflareResponse;
use crate::api_config::ApiConfig;
use crate::dns_record::DnsRecord;
use crate::dns_record_api_error::DnsRecordApiError;

#[derive(Debug)]
pub struct DnsRecordApi {
    pub config: ApiConfig,
}

impl From<ApiConfig> for DnsRecordApi {
    fn from(config: ApiConfig) -> Self {
        DnsRecordApi { config }
    }
}

impl DnsRecordApi {
    #[allow(unused)]
    pub fn create(&self, name: &str, content: &str) -> Result<CloudflareResponse<Value>, DnsRecordApiError> {
        let name = format!("{}.{}", name, self.config.domain);
        let url = format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records", self.config.zone_id);
        ureq::post(url.as_str())
            .set("Authorization", format!("Bearer {}", self.config.access_token).as_str())
            .set("Content-Type", "application/json")
            .send_string(format!(r#"{{
              "type": "A",
              "name": "{}",
              "content" : "{}",
              "ttl": 1
            }}"#, name, content).as_str())
            .map_err(|error| DnsRecordApiError::from(error))
            .map(|response| {
                response.into_json()
                    .expect("JSON parse into CloudflareResponse failed")
            })
    }

    #[allow(unused)]
    pub fn list(&self) -> CloudflareResponse<Vec<DnsRecord>> {
        let url = format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records", self.config.zone_id);
        ureq::get(url.as_str())
            .set("Authorization", format!("Bearer {}", self.config.access_token).as_str())
            .set("Content-Type", "application/json")
            .call()
            .expect("List request failed")
            .into_json()
            .map(|json: CloudflareResponse<Vec<DnsRecord>>| {
                json.map_result(&mut |result| {
                    result.iter()
                        .cloned()
                        .filter(|record| {
                            record.name.ends_with(&self.config.domain)
                        })
                        .collect()
                })
            })
            .expect("JSON parse into CloudflareResponse<Vec<DnsRecord>> failed")
    }

    #[allow(unused)]
    pub fn find_one_by_name(&self, name: &str) -> CloudflareResponse<Option<DnsRecord>> {
        let name = format!("{}.{}", name, self.config.domain);
        let url = format!(
            "https://api.cloudflare.com/client/v4/zones/{}/dns_records?name={}",
            self.config.zone_id,
            name
        );
        ureq::get(url.as_str())
            .set("Authorization", format!("Bearer {}", self.config.access_token).as_str())
            .set("Content-Type", "application/json")
            .call()
            .expect("List request failed")
            .into_json()
            .map(|vec: CloudflareResponse<Vec<DnsRecord>>| {
                vec.map_result(&mut |mut result: Vec<DnsRecord>| {
                    match result.len() {
                        0 => Option::None,
                        _ => Option::Some(result.remove(0)),
                    }
                })
            })
            .expect("JSON parse into CloudflareResponse<Vec<DnsRecord>> failed")
    }

    #[allow(unused)]
    pub fn update(&self, name: &str, content: &str) -> Result<CloudflareResponse<Value>, DnsRecordApiError> {
        let id = match self.find_one_by_name(name).result {
            None => {
                return Result::Err(DnsRecordApiError::Other("Dns Record not found"));
            }
            Some(result) => result.id
        };

        let url = format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}",
                          self.config.zone_id,
                          id);
        ureq::put(url.as_str())
            .set("Authorization", format!("Bearer {}", self.config.access_token).as_str())
            .set("Content-Type", "application/json")
            .send_string(format!(r#"{{
              "type": "A",
              "name": "{}.{}",
              "content" : "{}",
              "ttl": 1
            }}"#, name, self.config.domain, content).as_str())
            .map_err(|error| DnsRecordApiError::from(error))
            .map(|response| {
                response.into_json()
                    .expect("JSON parse into CloudflareResponse failed")
            })
    }

    #[allow(unused)]
    pub fn create_or_update(&self, name: &str, content: &str) -> Result<CloudflareResponse<Value>, DnsRecordApiError> {
        if let Ok(e) = self.update(name, content) {
            return Ok(e);
        }
        return self.create(name, content);
    }
}